# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Abono',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('cantidad', models.FloatField()),
                ('fecha', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='Pago',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('cantidad', models.FloatField()),
                ('fecha', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='Tipo_Abono',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('tipo', models.CharField(max_length=50)),
                ('descripcion', models.TextField()),
                ('fecha', models.DateField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Tipo_Pago',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('tipo', models.CharField(max_length=50)),
                ('descripcion', models.TextField()),
                ('fecha', models.DateField(auto_now=True)),
            ],
        ),
        migrations.AddField(
            model_name='pago',
            name='tipo',
            field=models.ForeignKey(to='balance.Tipo_Pago'),
        ),
        migrations.AddField(
            model_name='abono',
            name='tipo',
            field=models.ForeignKey(to='balance.Tipo_Abono'),
        ),
    ]
