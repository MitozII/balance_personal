# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('balance', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='tipo_abono',
            old_name='tipo',
            new_name='tipoa',
        ),
        migrations.RenameField(
            model_name='tipo_pago',
            old_name='tipo',
            new_name='tipop',
        ),
        migrations.AlterField(
            model_name='tipo_abono',
            name='descripcion',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='tipo_pago',
            name='descripcion',
            field=models.TextField(blank=True),
        ),
    ]
