from django.contrib import admin
from .models import *

#Configuracion de los tipos de abonos en el administrador
class TipoAbonoAdmin(admin.ModelAdmin):
    list_display = ('tipoa', 'descripcion', 'fecha')
    search_fields = ('tipoa',)

#Configuracion de los tipos de Pagos en el administrador
class TipoPagoAdmin(admin.ModelAdmin):
    list_display = ('tipop', 'descripcion', 'fecha')
    search_fields = ('tipop',)

#Configuracion de los Abonos en el administrador
class AbonoAdmin(admin.ModelAdmin):
    list_display = ('tipo', 'cantidad', 'fecha')
    search_fields = ('tipo__tipoa',)
    list_filter = ('fecha',)
    #si quiero ver una barra tipo nav con los filtros de
    #fecha uso 'date_hierarchy' en lugar de 'list_filter'
    date_hierarchy = 'fecha'
    ordering = ('fecha',)

#Configuracion de los Pagos en el administrador
class PagoAdmin(admin.ModelAdmin):
    list_display = ('tipo', 'cantidad', 'fecha')
    search_fields = ('tipo__tipop',)
    list_filter = ('fecha',)
    date_hierarchy = 'fecha'
    ordering = ('fecha',)

#Registracion de los modelos en el administrador de django
admin.site.register(Tipo_Abono, TipoAbonoAdmin)
admin.site.register(Tipo_Pago, TipoPagoAdmin)
admin.site.register(Abono, AbonoAdmin)
admin.site.register(Pago, PagoAdmin)