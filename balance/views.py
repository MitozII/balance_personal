from django.shortcuts import render, get_object_or_404
from .models import Pago

# Create your views here.

def index(request):
    pagos_detalle = Pago.objects.all()
    return render(request, 'balance/index.html', {'pagos_detalle':pagos_detalle})