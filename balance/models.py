from django.db import models

# Create your models here.


#Modelo de los Tipos de Abonos que se podran elegir
class Tipo_Abono(models.Model):
    tipoa = models.CharField(max_length=50)
    descripcion = models.TextField(blank=True)
    fecha = models.DateField(auto_now=True)

    def __str__(self):
        return self.tipoa

#Modelo de los Tipos de APagos que se podran elegir
class Tipo_Pago(models.Model):
    tipop = models.CharField(max_length=50)
    descripcion = models.TextField(blank=True)
    fecha = models.DateField(auto_now=True)

    def __str__(self):
        return self.tipop

#Modelo de los Tipos de Abonos que se registraran en la app
class Abono(models.Model):
    tipo = models.ForeignKey(Tipo_Abono)
    cantidad = models.FloatField()
    fecha = models.DateField()

    def __str__(self):
        return '%s %d %s' %(self.tipo, self.cantidad, self.fecha)

#Modelo de los Tipos de Pagos que se registraran en la app
class Pago(models.Model):
    tipo = models.ForeignKey(Tipo_Pago)
    cantidad = models.FloatField()
    fecha = models.DateField()

    def __str__(self):
        return '%s %d %s' %(self.tipo, self.cantidad, self.fecha)